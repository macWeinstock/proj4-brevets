"""
Nose tests for acp_times.py
"""

from acp_times import open_time, close_time
import arrow

import nose    # Testing framework
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

#Store a random global variable for time as an arrow object for comparisons
time = "2017-01-01T00:00"
timearrow = arrow.get(time)

def test_negative_control():
    """
    Test a negative control
    """
    assert open_time(-1, 200, time) == False
    assert close_time(-1, 200, time) == False


def test_too_large_control():
    """
    Test too large a number for control
    """
    assert open_time(999999, 1000, time) == False
    assert close_time(999999, 1000, time) == False


def test_zero_control():
    """
    Test 0 for control distance
    """
    assert open_time(0, 1000, time) == timearrow.isoformat()
    assert close_time(0, 1000, time) == timearrow.shift(hours=+1).isoformat()

def test_too_large_for_brevet():
    assert open_time(350, 200, time) == False
    assert close_time(350, 200, time) == False

def test_small_control():
    """
    Test a non-edge case small control time distance
    """
    print(open_time(100, 200, time))
    assert open_time(100, 200, time) == timearrow.shift(hours=+2, minutes=+56).isoformat()
    assert close_time(100, 200, time) == timearrow.shift(hours=+6, minutes=+40).isoformat()


def test_med_control():
    """
    Test a non-edge case medium control distance
    """
    assert open_time(467, 600, time) == timearrow.shift(hours=+14, minutes=+22).isoformat()
    assert close_time(467, 600, time) == timearrow.shift(hours=+31, minutes=+8).isoformat()

def test_large_control():
    """
    Test a non-edge case large control distance
    """
    assert open_time(985, 1000, time) == timearrow.shift(hours=+32, minutes=+33).isoformat()
    assert close_time(985, 1000, time) == timearrow.shift(hours=+73, minutes=+41).isoformat()


def test_edge_small_control():
    """
    Test an edge case small control distance
    """
    assert open_time(195, 200, time) == timearrow.shift(hours=+5, minutes=+44).isoformat()
    assert close_time(195, 200, time) == timearrow.shift(hours=+13, minutes=+0).isoformat()

    assert open_time(200, 200, time) == timearrow.shift(hours=+5, minutes=+53).isoformat()
    assert close_time(200, 200, time) == timearrow.shift(hours=+13, minutes=+30).isoformat()

    assert open_time(205, 200, time) == timearrow.shift(hours=+5, minutes=+53).isoformat()
    assert close_time(205, 200, time) == timearrow.shift(hours=+13, minutes=+30).isoformat()

def test_edge_medium_control():
    """
    Test an edge case medium control distance
    """
    assert open_time(595, 600, time) == timearrow.shift(hours=+18, minutes=+38).isoformat()
    assert close_time(595, 600, time) == timearrow.shift(hours=+39, minutes=+40).isoformat()

    assert open_time(600, 600, time) == timearrow.shift(hours=+18, minutes=+48).isoformat()
    assert close_time(600, 600, time) == timearrow.shift(hours=+40, minutes=+0).isoformat()

    assert open_time(605, 600, time) == timearrow.shift(hours=+18, minutes=+48).isoformat()
    assert close_time(605, 600, time) == timearrow.shift(hours=+40, minutes=+0).isoformat()


def test_edge_large_control():
    """
    Test an edge case large control distance
    """
    assert open_time(995, 1000, time) == timearrow.shift(hours=+32, minutes=+54).isoformat()
    assert close_time(995, 1000, time) == timearrow.shift(hours=+74, minutes=+34).isoformat()

    assert open_time(1000, 1000, time) == timearrow.shift(hours=+33, minutes=+5).isoformat()
    assert close_time(1000, 1000, time) == timearrow.shift(hours=+75, minutes=+0).isoformat()

    assert open_time(1005, 1000, time) == timearrow.shift(hours=+33, minutes=+5).isoformat()
    assert close_time(1005, 1000, time) == timearrow.shift(hours=+75, minutes=+0).isoformat()

